#!/usr/bin/env bash
echo 'Activating virtualenv'
source venv/bin/activate
echo 'Starting run'
./project.py
echo 'Done'
