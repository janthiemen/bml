#!/usr/bin/env python2

import argparse
import pprint

parser = argparse.ArgumentParser(description='Training models for Mercari price suggestion.')
parser.add_argument( '-m', '--model', metavar='model', nargs=1, choices=['ridge', 'elasticnet'], required=True)
args = parser.parse_args()

pp = pprint.PrettyPrinter(indent=2)
pp.pprint(args)