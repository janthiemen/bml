#!/usr/bin/env bash
rm -R ./venv/
virtualenv venv
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    ./venv/bin/pip install -r 'requirements.txt'
elif [[ "$OSTYPE" == "msys" ]]; then
    ./venv/Scripts/pip install -r 'requirements.txt'
fi
