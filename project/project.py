#!/usr/bin/env python3

# Mecari price suggestion
## Setup
### Imports
import pandas as pd
import numpy as np
import pandas as pd
import scipy as sp
import matplotlib.pyplot as plt

#### Import the SciKit models and utils
from sklearn.linear_model import Ridge, RidgeCV, ElasticNet, ElasticNetCV
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer,TfidfVectorizer, HashingVectorizer

import category_encoders as ce
import pprint

pp = pprint.PrettyPrinter(indent=2)

#### Two modes, one for Kaggle, one for local
MODE = 'local'
# MODE = 'kaggle'

TRAIN_DATA = '../input/train.tsv' if MODE == 'kaggle' else 'train.tsv'
TEST_DATA = '../input/test.tsv'

### Load data
train = pd.read_csv(TRAIN_DATA, sep='\t', engine='python')
train_prices = train.price
train_data = train.drop("price",axis=1)

if (MODE == 'local'):
    #Import the train and test sets. The engine is required because the test set would otherwise produce an error
    train_data, test_data, train_prices, test_prices = train_test_split(train, train_prices, test_size =0.3)
else:
    #In case of Kaggle the files are in the ../input directory, so uncomment these;
    test_data = pd.read_csv(TEST_DATA, sep='\t', engine='python')
    test_prices = None

data = (train_data, test_data, train_prices, test_prices)


### Describe the data
print(train_data.shape)
print(train_data.dtypes)
print()
print(train_prices.shape)
print(train_prices.dtypes)

## Preprocessing
### Item Conditioning
#### TODO: test ordinal version
def item_condition_ohe(data):
    train_data, test_data, train_prices, test_prices = data
    ohe = preprocessing.OneHotEncoder(sparse=True)
    train_ic_ohe = ohe.fit_transform(train_data["item_condition_id"].values.reshape((train_data.shape[0],1)))
    test_ic_ohe = ohe.transform(test_data["item_condition_id"].values.reshape((test_data.shape[0],1)))
    return (train_ic_ohe, test_ic_ohe)

### Brand
def brand_be(data):
    train_data, test_data, train_prices, test_prices = data
    # remove statistically insignificant brands to prevent overfitting
    train_brand = train_data.loc[:, ['brand_name']].fillna('unknown')
    test_brand = test_data.loc[:, ['brand_name']].fillna('unknown')
    brand_names, brand_counts = np.unique(train_brand,return_counts=True)
    brands_to_replace = [n for n, c in zip(brand_names, brand_counts) if c < 100]
    train_brand.replace(brands_to_replace, 'other', inplace=True)
    brands_to_replace = brands_to_replace + list(set(np.unique(test_brand)) - set(brand_names))
    test_brand.replace(brands_to_replace, 'other', inplace=True)

    # binary encode to reduce dimensions
    brand_encoder = ce.BinaryEncoder().fit(train_brand, train_prices)
    train_brand = brand_encoder.transform(train_brand)
    test_brand = brand_encoder.transform(test_brand)
    return (train_brand, test_brand)

### Category
def category_be(data):
    train_data, test_data, train_prices, test_prices = data
    import category_encoders as ce
    
    # remove statistically insignificant categories to prevent overfitting
    train_cat = train_data.category_name.fillna('unknown')
    test_cat = test_data.category_name.fillna('unknown')
    cat_names, cat_counts = np.unique(train_cat,return_counts=True)
    print(len(cat_names))
    cat_to_replace = [n for n, c in zip(cat_names, cat_counts) if c < 100]
    cat_replace_with = [x[0] + '/' + x[1] + '/' + 'other' for x in [cat.split('/') for cat in cat_to_replace]]
    # print(cat_replace_with[:5])
    train_cat.replace(cat_to_replace, cat_replace_with, inplace=True)
    test_cat.replace(cat_to_replace, cat_replace_with, inplace=True)

    cat_names = np.unique(train.category_name)
    print(len(cat_names))

    # binary encode to reduce dimensions
    cat_encoder = ce.BinaryEncoder(cols=["category_name"]).fit(train, train_price)
    train = cat_encoder.transform(train)
    print(train_cat.dtypes)
    return (train_cat, test_cat)

### Pet free
def pet_free(data):
    train_data, test_data, train_prices, test_prices = data
    train_pet = train_data.item_description.str.contains(pat="pet free", case=False, regex=False, na=False).astype(int)
    test_pet = test_data.item_description.str.contains(pat="pet free", case=False, regex=False, na=False).astype(int)
    return (train_pet, test_pet)

### Smoke free
def smoke_free(data):
    train_data, test_data, train_prices, test_prices = data
    train_smoke = train_data.item_description.str.contains(pat="smoke free", case=False, regex=False, na=False).astype(int)
    test_smoke = test_data.item_description.str.contains(pat="smoke free", case=False, regex=False, na=False).astype(int)
    return (train_smoke, test_smoke)

### No description yet
def no_description(data):
    train_data, test_data, train_prices, test_prices = data
    train_no_desc = train_data.item_description.str.contains(pat="No description yet", case=False, regex=False, na=False).astype(int)
    test_no_desc = test_data.item_description.str.contains(pat="No description yet", case=False, regex=False, na=False).astype(int)
    return (train_no_desc, test_no_desc)

### Name
def name_tf_idf(data):
    train_data, test_data, train_prices, test_prices = data
    train_name = train_data.name.fillna('')
    test_name = test_data.name.fillna('')
    vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5, stop_words='english')
    train_td_idf = vectorizer.fit_transform(train_name)
    test_td_idf = vectorizer.transform(test_name)
    return (train_td_idf, test_td_idf)

### Description
def description_tf_idf(data):
    train_data, test_data, train_prices, test_prices = data
    train_description = train_data.item_description.fillna('')
    test_description = test_data.item_description.fillna('')
    vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5, stop_words='english')
    train_description = vectorizer.fit_transform(train_description)
    test_description = vectorizer.transform(test_description)
    return (train_description, test_description)

### Price
def price(data):
    train_data, test_data, train_prices, test_prices = data
    return train_prices.transform(np.log1p)

### Do the actual preprocessing
def preprocess(data):
    train_data, test_data, train_prices, test_prices = data
    active_features = set([item_condition_ohe, brand_be, pet_free, smoke_free, no_description, name_tf_idf, description_tf_idf])
    
    features = [feature(data) for feature in active_features]
    training = [sp.sparse.csr_matrix(x[0]) for x in features]
    testing = [sp.sparse.csr_matrix(x[1]) for x in features]
    training = tuple(t if t.shape[0] == train_data.shape[0] else t.transpose() for t in training)
    testing = tuple(t if t.shape[0] == test_data.shape[0] else t.transpose() for t in testing)
    
    processed_train_data = sp.sparse.hstack(training, format='csr')
    processed_test_data = sp.sparse.hstack(testing, format='csr')
    processed_train_prices = price(data)
    return (processed_train_data, processed_test_data, processed_train_prices, processed_test_prices)

## Run model
preprocessed = preprocess((train_data, test_data, train_prices, test_prices))

def rmsle(real, predicted):
    return np.sqrt(np.mean(np.power(np.log1p(real)-np.log1p(predicted), 2)))

def evaluate(model, data):
    train_data, test_data, train_prices, test_prices = data
    if MODE == 'local':
        model.fit(train_data,train_prices)
        pred_prices = model.predict(test_data)
        
        pred_prices = np.exp(pred_prices) - 1
        
        # Calculate the error measures
        mse = np.mean((pred_prices - test_prices)**2)
        rse = model.score(test_data,test_prices)
        error_rmsle = rmsle(test_prices, pred_prices)
        print("Mean Square Error: ", mse)
        print("r-Square score: ", rse)        
        print("RMSLE: ", error_rmsle)
        
    else:
        model.fit(train_features, train_prices)
        pred_prices = model.predict()

def ridge(alpha, data):
    evaluate(Ridge(alpha, copy_X=True), data)

def ridgecv(data):
    evaluate(RidgeCV(), data)

def elasticnet(lasso, ridge, data):
    evaluate(ElasticNet(alpha=lasso, l1_ratio=ridge, copy_X=True), data)

def elasticnetcv(l1_ratio, data):
    evaluate(ElasticNetCV(l1_ratio=l1_ratio, copy_X=True, n_jobs=-1), data)


# print("Ridge")
# ridge(0.5, preprocessed)

# print("Elastic Net")
# elasticnet(0.5, 0.5, preprocessed)

# ratio = [.1, .5, .7, .9, .95, .99, 1]
# print("Elastic Net CV")
# en_cv = elasticnetcv(ratio, preprocessed)
# print(en_cv.alpha_)
# print(en_cv.l1_ratio_)

print("Ridge CV")
r_cv = ridgecv(data)
print(r_cv.alpha_)