#!/usr/bin/env python2

# Mercari price suggestion
## Setup
### Imports
import pandas as pd
import numpy as np
import pandas as pd
import scipy as sp

from sklearn.linear_model import Ridge, RidgeCV, ElasticNet, ElasticNetCV
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer,TfidfVectorizer, HashingVectorizer

import category_encoders as ce

import pprint

### Loading the data
def load_data(mode='local', train_file='train.tsv', test_file='test.tsv'):
    ### Load data
    train = pd.read_csv(train_file, sep='\t', engine='python')
    train_prices = train.price
    train_data = train.drop("price",axis=1)

    if (mode == 'local'):
        #Import the train and test sets. The engine is required because the test set would otherwise produce an error
        train_data, test_data, train_prices, test_prices = train_test_split(train, train_prices, test_size =0.3)
    else:
        #In case of Kaggle the files are in the ../input directory, so uncomment these;
        test_data = pd.read_csv(test_file, sep='\t', engine='python')
        test_prices = None

    return (train_data, test_data, train_prices, test_prices)

### Describe the data
def describe_data(data):
    train_data, test_data, train_prices, test_prices = data
    ### Describe the data
    print(train_data.shape)
    print(train_data.dtypes)
    print()
    print(train_prices.shape)
    print(train_prices.dtypes)


## Preprocessing
### Item Conditioning
#### TODO: test ordinal version
def item_condition_ohe(data):
    train_data, test_data, train_prices, test_prices = data
    ohe = preprocessing.OneHotEncoder(sparse=True)
    train_ic_ohe = ohe.fit_transform(train_data["item_condition_id"].values.reshape((train_data.shape[0],1)))
    test_ic_ohe = ohe.transform(test_data["item_condition_id"].values.reshape((test_data.shape[0],1)))
    return (train_ic_ohe, test_ic_ohe)


### Brand
def brand_be(data):
    train_data, test_data, train_prices, test_prices = data
    # remove statistically insignificant brands to prevent overfitting
    train_brand = train_data.loc[:, ['brand_name']].fillna('unknown')
    test_brand = test_data.loc[:, ['brand_name']].fillna('unknown')
    brand_names, brand_counts = np.unique(train_brand,return_counts=True)
    brands_to_replace = [n for n, c in zip(brand_names, brand_counts) if c < 100]
    train_brand.replace(brands_to_replace, 'other', inplace=True)
    brands_to_replace = brands_to_replace + list(set(np.unique(test_brand)) - set(brand_names))
    test_brand.replace(brands_to_replace, 'other', inplace=True)

    # binary encode to reduce dimensions
    brand_encoder = ce.BinaryEncoder().fit(train_brand, train_prices)
    train_brand = brand_encoder.transform(train_brand)
    test_brand = brand_encoder.transform(test_brand)
    return (train_brand, test_brand)


### Category
def category_be(data):
    train_data, test_data, train_prices, test_prices = data
    
    # remove statistically insignificant categories to prevent overfitting
    train_cat = train_data.category_name.fillna('unknown')
    test_cat = test_data.category_name.fillna('unknown')
    cat_names, cat_counts = np.unique(train_cat,return_counts=True)
    print(len(cat_names))
    cat_to_replace = [n for n, c in zip(cat_names, cat_counts) if c < 100]
    cat_replace_with = [x[0] + '/' + x[1] + '/' + 'other' for x in [cat.split('/') for cat in cat_to_replace]]
    # print(cat_replace_with[:5])
    train_cat.replace(cat_to_replace, cat_replace_with, inplace=True)
    test_cat.replace(cat_to_replace, cat_replace_with, inplace=True)

    cat_names = np.unique(train.category_name)
    print(len(cat_names))

    # binary encode to reduce dimensions
    cat_encoder = ce.BinaryEncoder(cols=["category_name"]).fit(train, train_price)
    train = cat_encoder.transform(train)
    print(train_cat.dtypes)
    return (train_cat, test_cat)


### helper method for pet_free, smoke_free, no_description
def text_in_column(train_column, test_column, text):
    train = train_column.str.contains(pat=text, case=False, regex=False, na=False).astype(int)
    test = test_column.str.contains(pat=text, case=False, regex=False, na=False).astype(int)
    train = sp.sparse.csr_matrix(train).transpose()
    test = sp.sparse.csr_matrix(test).transpose()
    return (train, test)


### Pet free
def pet_free(data):
    train_data, test_data, train_prices, test_prices = data
    return text_in_column(train_data.item_description, test_data.item_description, "pet free")


### Smoke free
def smoke_free(data):
    train_data, test_data, train_prices, test_prices = data
    return text_in_column(train_data.item_description, test_data.item_description, "smoke free")


### No description yet
def no_description(data):
    train_data, test_data, train_prices, test_prices = data
    return text_in_column(train_data.item_description, test_data.item_description, "No description yet")


### helper for tf_idf for name and description
def tf_idf(train_column, test_column):
    train = train_column.fillna('')
    test = test_column.fillna('')
    vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5, stop_words='english')
    train_td_idf = vectorizer.fit_transform(train)
    test_td_idf = vectorizer.transform(test)
    return (train_td_idf, test_td_idf)


### Name
def name_tf_idf(data):
    train_data, test_data, train_prices, test_prices = data
    return tf_idf(train_data.name, test_data.name)


### Description
def description_tf_idf(data):
    train_data, test_data, train_prices, test_prices = data
    return tf_idf(train_data.item_description, test_data.item_description)


def shipping(data):
    train_data, test_data, train_prices, test_prices = data
    train = sp.sparse.csr_matrix(train_data.shipping).transpose()
    test = sp.sparse.csr_matrix(test_data.shipping).transpose()
    return (train, test)

### Price
def price(data):
    train_data, test_data, train_prices, test_prices = data
    return train_prices.transform(np.log1p)


### Do the actual preprocessing
def preprocess(data):
    train_data, test_data, train_prices, test_prices = data
    active_features = set([item_condition_ohe, brand_be, pet_free, smoke_free, no_description, name_tf_idf, description_tf_idf, shipping])
    
    features = [feature(data) for feature in active_features]
    # os.path.exists(filename)
    training = tuple(t[0] for t in features)
    testing = tuple(t[1] for t in features)
    
    processed_train_data = sp.sparse.hstack(training, format='csr')
    processed_test_data = sp.sparse.hstack(testing, format='csr')
    processed_train_prices = price(data)
    return (processed_train_data, processed_test_data, processed_train_prices, test_prices)



def rmsle(real, predicted):
    return np.sqrt(np.mean(np.power(np.log1p(real)-np.log1p(predicted), 2)))


def mse(real, predicted):
    return np.mean((predicted - real)**2)
        

def evaluate(model, data, error_measures=(mse, rmsle), mode='local', out_file='out.csv'):
    train_data, test_data, train_prices, test_prices = data

    model.fit(train_data, train_prices)
    pred_prices = model.predict(test_data)
    pred_prices = np.exp(pred_prices) - 1
    
    error_scores = ()

    if mode == 'local':
        test_prices = np.exp(test_prices) - 1
        error_scores = tuple(e(pred_prices, test_prices) for e in error_measures)

    if mode == 'kaggle':
        with open(out_file, 'w') as out:
            out.write("test_id,price\n")
            for i, prediction in enumerate(pred_prices):
                out.write(str(i) + ',' + str(prediction) + '\n')

    return (model, error_scores)


def ridge(alpha=0.5):
    return Ridge(alpha, copy_X=True)


def ridgecv():
    return RidgeCV()


def elasticnet(lasso=0.5, ridge=0.5):
    return ElasticNet(alpha=lasso, l1_ratio=ridge, copy_X=True)


def elasticnetcv(l1_ratio=[.1, .5, .7, .9, .95, .99, 1]):
    return ElasticNetCV(l1_ratio=l1_ratio, copy_X=True, n_jobs=-1)


def run(models, data, error_measures=(mse, rmsle), mode='local', out_file='out.csv'):
    return [evaluate(model, data, (), mode, out_file) for model in models]

# -------------------------------------------------------------------

MODE = 'kaggle'
TRAIN_FILE = '../input/train.tsv' if MODE == 'kaggle' else 'train.tsv'
TEST_FILE = '../input/test.tsv'
OUT_FILE = 'sample_submission.csv'

pp = pprint.PrettyPrinter(indent=2)

data = load_data(mode=MODE, train_file=TRAIN_FILE, test_file=TEST_FILE)
preprocessed_data = preprocess(data)

models = [ridgecv()]
results = run(models, preprocessed_data, (), MODE, OUT_FILE)

for result in results:
    pp.pprint(result)